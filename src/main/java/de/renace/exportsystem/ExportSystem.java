package de.renace.exportsystem;

import com.google.gson.Gson;
import de.renace.exportsystem.backend.DatabaseManager;
import de.renace.exportsystem.backend.ExportManager;
import de.renace.exportsystem.commands.ExportDataCommand;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ᅟ_____          _____               ᅟ_____                 ᅟ_
 * | ____| __  __ |_   _|   __ _   ___  |_   _|  _   _        __| |   ___
 * |  _|   \ \/ /   | |    / _` | / __|   | |   | | | |      / _` |  / _ \
 * | |___   >  <    | |   | (_| | \__ \   | |   | |_| |  _  | (_| | |  __/
 * |_____| /_/\_\   |_|    \__,_| |___/   |_|    \__, | (_)  \__,_|  \___|
 * ᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟ|___/
 * Copyright ©ExTasTy.de & _Avery 2016-2019    Erstellt von _Avery | Lukas am 2019-03-15 um 17:32
 **/

@Getter
public class ExportSystem extends Plugin {

    public static final String PREFIX = "§8➟ §b§lRenace§3§lDE §8┃ §7";

    private static ExportSystem instance;

    private final Gson gson = new Gson( );

    private ExecutorService executorService;

    private PluginManager pluginManager;

    private DatabaseManager databaseManager;

    private ExportManager exportManager;

    @Override
    public void onEnable( ) {
        /* set the instance */
        instance = this;

        /* setup mongodb */
        setupMongoDB( );

        /* fetch all classes */
        fetchClasses( );

        /* register the commands */
        registerCommands( );
    }

    @Override
    public void onDisable( ) {

    }

    /**
     * This method fetch the classes
     */
    private void fetchClasses( ) {
        /* initialize executorservice */
        executorService = Executors.newCachedThreadPool( );

        pluginManager = ProxyServer.getInstance( ).getPluginManager( );

        this.exportManager = new ExportManager( this );
    }

    /**
     * MongoDB connection
     */
    private void setupMongoDB( ) {
        this.databaseManager = new DatabaseManager( "localhost", 27017 );
        this.databaseManager.connect( );
    }

    /**
     * This method register all commands
     */
    private void registerCommands( ) {
        pluginManager.registerCommand( this, new ExportDataCommand( "exportdata", this, "dataexport", "data", "export" ) );
    }
}
