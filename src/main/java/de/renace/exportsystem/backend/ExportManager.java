package de.renace.exportsystem.backend;

import com.mongodb.Block;
import com.mongodb.client.model.Filters;
import de.renace.exportsystem.ExportSystem;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * ᅟ_____          _____               ᅟ_____                 ᅟ_
 * | ____| __  __ |_   _|   __ _   ___  |_   _|  _   _        __| |   ___
 * |  _|   \ \/ /   | |    / _` | / __|   | |   | | | |      / _` |  / _ \
 * | |___   >  <    | |   | (_| | \__ \   | |   | |_| |  _  | (_| | |  __/
 * |_____| /_/\_\   |_|    \__,_| |___/   |_|    \__, | (_)  \__,_|  \___|
 * ᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟ|___/
 * Copyright ©ExTasTy.de & _Avery 2016-2019    Erstellt von _Avery | Lukas am 2019-03-15 um 19:38
 **/
public class ExportManager {

    private final ExportSystem exportSystem;

    public ExportManager( ExportSystem exportSystem ) {
        this.exportSystem = exportSystem;
    }

    /**
     * @return
     */
    public List< String > getMongoCollections( String database ) {
        List< String > data = new ArrayList<>( );

        Block< String > block = ( String name ) -> {
            data.add( name );
        };

        exportSystem.getDatabaseManager( ).getDatabase( database ).listCollectionNames( ).forEach( block );

        return data;
    }

    /**
     * @param uuid
     * @param consumer
     */
    public void exportData( String uuid, Consumer< Map< Document, String > > consumer ) {
        exportSystem.getExecutorService( ).execute( ( ) -> {
            List< String > backendCollections = getMongoCollections( "backend" );
            List< String > statsCollections = getMongoCollections( "stats" );

            Map< Document, String > documents = new HashMap<>( );

            for ( String name : backendCollections ) {
                Document userDoc = exportSystem.getDatabaseManager( ).getDatabase( "backend" ).getCollection( name ).find(
                        Filters.eq( "uuid", uuid ) ).first( );
                documents.put( userDoc, name );
            }

            for ( String name : statsCollections ) {
                Document userDoc = exportSystem.getDatabaseManager( ).getDatabase( "stats" ).getCollection( name ).find(
                        Filters.eq( "uuid", uuid ) ).first( );
                documents.put( userDoc, name );
            }

            consumer.accept( documents );
        } );
    }


    /**
     * @param uuid
     * @param consumer
     */
    public void finishExport( String uuid, Consumer< String > consumer ) {
        exportData( uuid, ( data ) -> {
            List< String > srcFiles = new ArrayList<>( );

            data.forEach( ( document, name ) -> {
                if ( document != null ) {
                    try {
                        String hashedExport = generateHashExport( 9 );
                        String finalName = name.toLowerCase( ).replace( "_", "-" );

                        srcFiles.add( "/home/Exports/" + finalName + ":" + hashedExport + ".json" );

                        FileWriter file = new FileWriter( "/home/Exports/" + finalName + ":" + hashedExport + ".json" );
                        try {
                            JsonWriterSettings writerSettings = new JsonWriterSettings( JsonMode.SHELL, true );

                            document.remove( "_id" );
                            file.write( document.toJson( writerSettings ) );
                        } catch ( IOException ex ) {
                            ex.printStackTrace( );
                        } finally {
                            file.flush( );
                            file.close( );
                        }

                    } catch ( Exception ex ) {
                        consumer.accept( null );
                    }
                }
            } );

            try {
                String filename = uuid + "-" + generateHashExport( 9 ) + ".zip";
                FileOutputStream fileOutputStream = new FileOutputStream( "/home/Exports/" + filename );
                ZipOutputStream zipOutputStream = new ZipOutputStream( fileOutputStream );

                for ( String srcFile : srcFiles ) {
                    File fileToZip = new File( srcFile );
                    fileToZip.setReadable( true, false );
                    fileToZip.setWritable( true, false );
                    fileToZip.setExecutable( true, false );
                    FileInputStream fileInputStream = new FileInputStream( fileToZip );
                    ZipEntry zipEntry = new ZipEntry( fileToZip.getName( ) );
                    zipOutputStream.putNextEntry( zipEntry );

                    byte[] bytes = new byte[ 1024 ];
                    int length;
                    while ( ( length = fileInputStream.read( bytes ) ) >= 0 ) {
                        zipOutputStream.write( bytes, 0, length );
                    }
                    fileInputStream.close( );
                }
                zipOutputStream.close( );
                fileOutputStream.close( );

                for ( String srcFile : srcFiles ) {
                    File fileToDelete = new File( srcFile );
                    fileToDelete.setReadable( true, false );
                    fileToDelete.setWritable( true, false );
                    fileToDelete.setExecutable( true, false );
                    fileToDelete.delete( );
                }

                consumer.accept( "/home/Exports/" + filename );
            } catch ( IOException ex ) {
                ex.printStackTrace( );
                consumer.accept( null );
            }
        } );
    }

    /**
     * @param uuid
     * @param email
     * @param filePath
     */
    public void sendEmail( final String uuid, final String email, final String filePath ) {
        Properties prop = new Properties( );
        prop.put( "mail.smtp.auth", true );
        prop.put( "mail.smtp.starttls.enable", "true" );
        prop.put( "mail.smtp.host", "host" );
        prop.put( "mail.smtp.port", "25" );

        Session session = Session.getInstance( prop, new Authenticator( ) {
            @Override
            protected PasswordAuthentication getPasswordAuthentication( ) {
                return new PasswordAuthentication( "test@test.com", "testpw" );
            }
        } );

        try {
            Message message = new MimeMessage( session );
            message.setFrom( new InternetAddress( "test@test.com", "Renace - Privacy" ) );
            message.setRecipients(
                    Message.RecipientType.TO, InternetAddress.parse( email ) );
            message.setSubject( "Renace Network - Export System" );

            String msg =
                    "<div>Lieber Nutzer, <br/><br/>" +
                            "Testnachricht";

            MimeBodyPart mimeBodyMessagePart = new MimeBodyPart( );
            mimeBodyMessagePart.setHeader( "Content-Type", "text/html; charset=\"utf-8\"" );
            mimeBodyMessagePart.setContent( msg, "text/html; charset=\"utf-8\"" );

            MimeBodyPart attachPart = new MimeBodyPart( );
            attachPart.attachFile( new File( filePath ) );

            Multipart multipart = new MimeMultipart( );
            multipart.addBodyPart( mimeBodyMessagePart );
            multipart.addBodyPart( attachPart );

            message.setContent( multipart );

            Thread.currentThread( ).setContextClassLoader( getClass( ).getClassLoader( ) );

            Transport.send( message );
        } catch ( Exception ex ) {
            ex.printStackTrace( );
        }
    }

    /**
     * This method generates an random string with the given length
     *
     * @param length
     * @return the final random generated string
     */
    public String generateHashExport( final int length ) {
        String result = "";
        char[] chars = "QWEZUIAWSDKLYVBNM1234567890".toCharArray( );
        for ( int i = 0; i < length; i++ ) {
            result = result + chars[ new Random( ).nextInt( chars.length ) ];
        }
        return result;
    }
}
