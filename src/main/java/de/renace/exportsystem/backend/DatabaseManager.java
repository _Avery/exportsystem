package de.renace.exportsystem.backend;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import java.util.Arrays;

/**
 * ᅟ_____          _____               ᅟ_____                 ᅟ_
 * | ____| __  __ |_   _|   __ _   ___  |_   _|  _   _        __| |   ___
 * |  _|   \ \/ /   | |    / _` | / __|   | |   | | | |      / _` |  / _ \
 * | |___   >  <    | |   | (_| | \__ \   | |   | |_| |  _  | (_| | |  __/
 * |_____| /_/\_\   |_|    \__,_| |___/   |_|    \__, | (_)  \__,_|  \___|
 * ᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟ|___/
 * Copyright ©ExTasTy.de & _Avery 2016-2019    Erstellt von _Avery | Lukas am 2019-03-15 um 19:47
 **/
public class DatabaseManager {

    private final String hostname;
    private final int port;

    private MongoClient client;

    private MongoDatabase database;

    public DatabaseManager( String hostname, int port ) {
        this.hostname = hostname;
        this.port = port;
    }

    public void connect( ) {
        MongoCredential credential = MongoCredential.createCredential( "admin", "admin", "PASSWORD".toCharArray() );

        this.client = new MongoClient( new ServerAddress( this.hostname, this.port ),
                Arrays.asList( credential ) );
    }

    public MongoClient getClient( ) {
        return this.client;
    }

    public MongoDatabase getDatabase( String name ) {
        return this.client.getDatabase( name );
    }

    public MongoCollection getCollection( String name ) {
        return this.database.getCollection( name );
    }

}
