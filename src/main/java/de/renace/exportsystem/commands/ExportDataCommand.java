package de.renace.exportsystem.commands;

import de.dytanic.cloudnet.api.CloudAPI;
import de.renace.bungeecore.BungeeCore;
import de.renace.exportsystem.ExportSystem;
import de.renace.renaceapi.player.NetworkPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ᅟ_____          _____               ᅟ_____                 ᅟ_
 * | ____| __  __ |_   _|   __ _   ___  |_   _|  _   _        __| |   ___
 * |  _|   \ \/ /   | |    / _` | / __|   | |   | | | |      / _` |  / _ \
 * | |___   >  <    | |   | (_| | \__ \   | |   | |_| |  _  | (_| | |  __/
 * |_____| /_/\_\   |_|    \__,_| |___/   |_|    \__, | (_)  \__,_|  \___|
 * ᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟᅟ|___/
 * Copyright ©ExTasTy.de & _Avery 2016-2019    Erstellt von _Avery | Lukas am 2019-03-15 um 20:39
 **/
public class ExportDataCommand extends Command {

    private final ExportSystem exportSystem;

    private final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile( "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE );

    public ExportDataCommand( String name, ExportSystem exportSystem, String... aliases ) {
        super( name, "", aliases );
        this.exportSystem = exportSystem;
    }

    @Override
    public void execute( CommandSender sender, String[] args ) {

        if ( sender instanceof ProxiedPlayer ) {
            final ProxiedPlayer player = ( ProxiedPlayer ) sender;

            if ( args.length == 1 ) {
                try {
                    String email = String.valueOf( args[ 0 ] );

                    if ( !player.hasPermission( "export.admin" ) ) {
                        NetworkPlayer networkPlayer = BungeeCore.getInstance( ).getCache( ).get( player.getUniqueId( ) );

                        if ( networkPlayer.getPrivacyDelay( ) > System.currentTimeMillis( ) ) {
                            player.sendMessage( ExportSystem.PREFIX + "§cDu kannst deine Daten nur jede 12 Stunden exportieren!" );
                            return;
                        }
                    }

                    if ( validate( email ) ) {
                        player.sendMessage( ExportSystem.PREFIX + "§aDer Export wird erstellt..." );

                        exportSystem.getExportManager( ).finishExport( player.getUniqueId( ).toString( ), ( callback ) -> {
                            if ( callback != null ) {
                                exportSystem.getExportManager( ).sendEmail( player.getUniqueId( ).toString( ), email, callback );

                                Executors.newSingleThreadScheduledExecutor( ).scheduleWithFixedDelay( ( ) -> {
                                    try {
                                        File fileToDelete = new File( callback );
                                        fileToDelete.delete( );
                                    } catch ( Exception ex ) {
                                        ex.printStackTrace( );
                                    }
                                }, 60, 60, TimeUnit.SECONDS );

                                player.sendMessage( ExportSystem.PREFIX + "§aDer Export war erfolgreich und wurde an die E-Mail Adresse §e" + email + " §agesendet!" );
                                player.sendMessage( ExportSystem.PREFIX + "§7Bitte beachte, dass die E-Mail eine gewisse Zeit braucht, bis diese bei dir ist. Solltest du diese nach ca. §c12 Stunden §7noch immer nicht erhalten haben, bitte melde dich im §cSupport§7! Überprüfe auch deinen §cSpam-Ordner§7." );

                                if ( !player.hasPermission( "export.admin" ) ) {
                                    NetworkPlayer networkPlayer = BungeeCore.getInstance( ).getCache( ).get( player.getUniqueId( ) );
                                    networkPlayer.setPrivacyDelay( System.currentTimeMillis( ) + 43200000L );

                                    BungeeCore.getInstance( ).getCache( ).cache( networkPlayer );
                                }
                            } else {
                                player.sendMessage( ExportSystem.PREFIX + "§cBeim Vorgang ist ein Fehler aufgetreten. Kontaktiere einen Administrator!" );
                            }
                        } );
                    } else {
                        player.sendMessage( ExportSystem.PREFIX + "§cBitte gib eine korrekte E-Mail Adresse an!" );
                    }
                } catch ( Exception ex ) {
                    player.sendMessage( ExportSystem.PREFIX + "§cBeim Vorgang ist ein Fehler aufgetreten. Kontaktiere einen Administrator!" );
                }
            } else if ( args.length == 2 ) {
                try {
                    String name = String.valueOf( args[ 0 ] );

                    if ( CloudAPI.getInstance( ).getOfflinePlayer( name ) == null ) {
                        player.sendMessage( ExportSystem.PREFIX + "§cDer Spieler war noch nie auf unserem Netzwerk!" );
                        return;
                    }

                    String uuid = CloudAPI.getInstance( ).getOfflinePlayer( name ).getUniqueId( ).toString( );
                    String email = String.valueOf( args[ 1 ] );

                    if ( player.hasPermission( "export.admin" ) ) {
                        if ( validate( email ) ) {
                            player.sendMessage( ExportSystem.PREFIX + "§aDer Export wird erstellt..." );

                            exportSystem.getExportManager( ).finishExport( uuid, ( callback ) -> {
                                if ( callback != null ) {
                                    exportSystem.getExportManager( ).sendEmail( player.getUniqueId( ).toString( ), email, callback );

                                    Executors.newSingleThreadScheduledExecutor( ).scheduleWithFixedDelay( ( ) -> {
                                        try {
                                            File fileToDelete = new File( callback );
                                            fileToDelete.delete( );
                                        } catch ( Exception ex ) {
                                            ex.printStackTrace( );
                                        }
                                    }, 60, 60, TimeUnit.SECONDS );

                                    player.sendMessage( ExportSystem.PREFIX + "§aDer Export war erfolgreich und wurde an die E-Mail Adresse §e" + email + " §agesendet!" );
                                    player.sendMessage( ExportSystem.PREFIX + "§7Bitte beachte, dass die E-Mail eine gewisse Zeit braucht, bis diese bei dir ist. Solltest du diese nach ca. §c12 Stunden §7noch immer nicht erhalten haben, bitte melde dich im §cSupport§7! Überprüfe auch deinen §cSpam-Ordner§7." );

                                    if ( !player.hasPermission( "export.admin" ) ) {
                                        NetworkPlayer networkPlayer = BungeeCore.getInstance( ).getCache( ).get( player.getUniqueId( ) );
                                        networkPlayer.setPrivacyDelay( System.currentTimeMillis( ) + 43200000L );

                                        BungeeCore.getInstance( ).getCache( ).cache( networkPlayer );
                                    }
                                } else {
                                    player.sendMessage( ExportSystem.PREFIX + "§cBeim Vorgang ist ein Fehler aufgetreten. Kontaktiere einen Administrator!" );
                                }
                            } );
                        } else {
                            player.sendMessage( ExportSystem.PREFIX + "§cBitte gib eine korrekte E-Mail Adresse an!" );
                        }
                    }
                } catch ( Exception ex ) {
                    player.sendMessage( ExportSystem.PREFIX + "§cBeim Vorgang ist ein Fehler aufgetreten. Kontaktiere einen Administrator!" );
                }
            } else {
                player.sendMessage( ExportSystem.PREFIX + "§7Richtige Verwendung: §c/exportdata <E-Mail>" );
            }
        }
    }

    private boolean validate( String emailStr ) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher( emailStr );
        return matcher.find( );
    }
}
